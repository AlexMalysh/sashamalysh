public class workerSalary extends users {

    private int expYears;
    private double salary;

    public workerSalary(String email, String pass, String lastName, String firstName, String workphone, String mobilePhone) {
        super(email, pass, lastName, firstName, workphone, mobilePhone);

    }

    public workerSalary(String email, String pass) {
        super(email, pass);

    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setExp(int exp) {
        this.expYears = exp;
    }

    public double getSalary() {

        return salary;
    }

    public void increaseSalary() {


        if (expYears <= 2) {
            salary = salary + salary*0.05;
        }
        if (expYears == 2 || expYears > 2 && expYears < 5 || expYears == 5) {
            salary = salary + salary*0.10;
        }
        if (expYears > 5) {
            salary = salary + salary*0.15;

        }
    }
}