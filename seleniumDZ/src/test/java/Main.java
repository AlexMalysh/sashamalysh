import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import java.util.List;

public class Main {

    WebDriver driver = new ChromeDriver();


    String email = "mail" + RandomStringUtils.randomNumeric(4) + "@gmail.com";
    String mobilePhone = "38050" + RandomStringUtils.randomNumeric(7);

    @BeforeClass
    public static void beforeClass() {
        final String path = String.format("%s/bin/chromedriver.exe",
                System.getProperty("user.dir"));
        System.setProperty("webdriver.chrome.driver", path);
    }

    @Before public void preCOnd(){
driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @Test
    public void firsttest() {

        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.id("first_name")).sendKeys("Gena");
        driver.findElement(By.id("last_name")).sendKeys("Petrov");
        driver.findElement(By.id("field_phone")).sendKeys(mobilePhone);
        driver.findElement(By.id("field_email")).sendKeys(email);
        driver.findElement(By.id("field_work_phone")).sendKeys("123456");
        driver.findElement(By.id("field_password")).sendKeys("ADS123ddd");
        driver.findElement(By.id("male")).click();
        WebElement testDropDown = driver.findElement(By.id("position"));
        Select dropdown = new Select(testDropDown);
        dropdown.selectByValue("manager");
        driver.findElement(By.id("button_account")).click();

    }

    @After
    public void ending
        (){

       driver.navigate().to("https://user-data.hillel.it/html/registration.html");

       driver.findElement(By.cssSelector(".authorization")).click();
       driver.findElement(By.id("email")).sendKeys(email);
       driver.findElement(By.id("password")).sendKeys("ADS123ddd");
       driver.findElement(By.cssSelector(".login_button")).click();
       driver.findElement(By.linkText("Employees")).click();
       driver.findElement(By.id("mobile_phone")).sendKeys(mobilePhone);
       driver.findElement(By.id("search")).click();

        WebElement actResult =driver.findElement(By.id("table"));
       /* System.out.println(actResult.getText());*/
        final String expectedName = "Gena";
        Assert.assertTrue(actResult.getText().contains(expectedName));
        Assert.assertTrue(actResult.getText().contains(mobilePhone));
        Assert.assertTrue(actResult.getText().contains(email));

    }


    }

